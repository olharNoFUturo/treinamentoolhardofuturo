## Apresentação do Curso Preparatório para o Estágio

### Módulo Introdução Gitlab

Duração de Leitura: 1h
Objetivo: Treinar o aluno nas melhores práticas e ferramentas mais usadas no mercado de TI.
Ferramentas: Git e Gitlab

### Aula Inicia
* Aula dia 08/02 - Realizada pelo Alan (Intrudição ao git e gitlab)
* Revisão com o material de apoio(1 dia de leitura)

### Material de Apoio:

* [Download e Instalação do Git](https://git-scm.com/downloads)
* [Curso GitLab Beginner Tutorial](https://www.youtube.com/watch?v=Jt4Z1vwtXT0&list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_)
* [GitLab Docs Tutorial](https://docs.gitlab.com/ee/README.html)
* [Start using Git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)



Tempo do Módulo: 01 dias corridos do início do curso