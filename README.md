## Olhar no Futuro

A Startup Olhar No Futuro é formada por especialistas em tecnologia da
informação e de mercado com 25 anos de experiência, estagiários e
iniciantes, foi idealizada para atender, facilitar e suprir as necessidades
diárias do público contemporâneo.

Objetivo do Curso: Formar profissional para:

* Atender às demandas da Olhar no Futuro e do Mercado;

* Direcionar o novo Perfil;

* Otimizar Tempo, Custo, Retrabalho e Evitar Desperdício;

* Colaborar com a Inovação, Praticidade, Viabilidade e Sustentabilidade
do Ecossistema.

### Adquirir Experiência

* Em ferramentas e metodologias usadas no mercado;

* Ter maior visibilidade como funciona um projeto de TI na
prática;

* Trabalho em Equipe;

* Trabalho Home Office;

* Melhores Práticas;

* Metodologia Ágil;

* Banco de Dados;

* Backend (ferramentas);

* Frontend;

Após avaliação de desempenho e o estagiário tirar as
certificações dos cursos solicitados pela Equipe Olhar no
Futuro, o mesmo receberá “Certificado de Conclusão de
Estágio” em nome da empresa.

### Regra do Estágio Obrigatório e não Obrigatório para selecionado(s)

* O Estágio será não remunerado;

* O Estagiário deve permanecer até o final do projeto;

* Cumprir pelo menos 4h de estágio por dia;

* Estudar e fazer os curso lhes atribuídos;

* Ficar online na plataforma Bitrix24, caso houver a necessidade de alinhamento com o estagiário;

* Fazer os cursos obrigatórios e ao final do estágio deverá apresentar os certificados;

* Frequentar as aulas de inglês;

* Importante: O não cumpromento de algum dos itens acima, não assinaremos o ESTÁGIO.

### Regras de Uso do Material

* O material de apresentação é de uso exclusivo do Curso
Preparatório para o Estágio;

* O material de apresentação é de propriedade da Olhar no
Futuro, portanto fica expressamente proibido fazer cópia,
download e entre outros;

* Os inscritos usarão o material no googles sala de aula, onde
conseguem acessar os cursos nos respectivos links
disponibilizados para cada módulo.