## Apresentação do Curso Preparatório para o Estágio

### Dicas de Trabalho Home Office

Duração de Leitura: 3h
Objetivo: Orientar e dar dicas ao aluno nas melhores práticas sobre o trabalho home office;

### Material de Estudo:

* [Ser mais produtivo](https://computerworld.com.br/sem-categoria/seis-dicas-para-ser-mais-produtivo-no-home-office/)
* [Rotina eficiente](https://papodehomem.com.br/home-office-como-se-organizar-para-ter-uma-rotina-eficiente-de-trabalho/)
* [Se organizar no trabalho Remoto](https://blog.unyleya.edu.br/guia-de-carreiras/como-se-organizar-para-o-regime-de-trabalho-remoto/)


Tempo de Leitura: 1 dia
