**1) Elaborar um modelo lógico e físico para uma seguradora de automóveis**

**Entidades:** Cliente, Apólice, Carro e Acidentes.


**Requisitos:**

1. Um cliente pode ter várias apólices (no mínimo uma), porem, uma apólice só pertence a um cliente;
2. Cada apólice somente dá cobertura a um carro e um carro só tem uma apólice na seguradora; 
3. Um carro pode ter zero ou n registros de acidentes a ele e um acidente só esta relacionado a um único carro.


**Atributos:**

- [ ] 1. Cliente: Número(id), Nome e Endereço;
- [ ] 2. Apólice: Número(id) e Valor;
- [ ] 3. Carro: Registro(id) e Marca;
- [ ] 4. Acidente: Data, Hora e Local;//O Acidente precisa de um identificador 

**Obs: O numero no atributo é referente ao seu codigo de identificação. Exemplo: número do Cliente = idCliente.**



**2) Elaborar um modelo lógico e físico para uma Indústria.**

**Entidades:** Peças, Depósitos, Fornecedor, Projeto, Funcionário e Departamento.

**Requisitos:**

1. Cada Funcionário pode estar alocado a somente um Departamento e um Departamento possui varios funcionarios;
2. Cada Funcionário pode pertencer a mais de um Projeto e um Projeto pode ter varios funcionarios;
3. Um projeto pode utilizar-se de vários Fornecedores e de várias Peças e uma peça e um fornecedor podem estar em varios projetos;
4. Uma Peça pode ser fornecida por vários Fornecedores e atender a vários Projetos;
5. Um Fornecedor pode atender a vários Projetos e fornecer várias Peças;
6. Um Depósito pode conter várias Peças e a industria específica só possui um Depósito;
7. Deseja-se ter um controle das peças utilizadas por cada Projeto, identificando inclusive
o seu Fornecedor. Gravar as informações de data de Início e Horas Trabalhadas no Projeto.(Incluir esses campos dentro do relacionamento correspondente).

**Atributos:**

- [ ] Peças: Número, Peso e Cor;
- [ ] Depósito: Número e Endereço;
- [ ] Fornecedor: Número e Endereço;
- [ ] Projeto: Número e Orçamento;
- [ ] Funcionário: Número, Salário e Telefone;
- [ ] Departamento: Número e Setor. 

**Obs: O numero no atributo é referente ao seu codigo de identificação. Exemplo: número do Depósito = idDeposito.**
