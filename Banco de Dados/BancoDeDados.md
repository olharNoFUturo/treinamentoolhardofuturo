## Apresentação do Curso Preparatório para o Estágio

### Módulo Banco de dados MYSQL

Duração de Leitura: 40h
Objetivo: Treinar o aluno nas melhores práticas de banco de dados.

* Modelo lógico;
* Modelo físico; 
* Atributos;
* Entidade relacionamentos.


### Aula Inicia
* Dia 11/02 - Aula de Mysql do curso em video;
* Aula dia 15/02 - Realizada pelo Caio (Intrudição ao Msql);
* Dia 16/02 - atividade - Entrega: 19/02.

### Material de Apoio:

 [Banco de Dados com MySQL](https://www.cursoemvideo.com/course/mysql/)


Tempo do Módulo: 10 dias corridos do início do curso