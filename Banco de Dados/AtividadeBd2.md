**1) Elaborar um diagrama E-R de um consultório clínico**

**Entidades**: Médico, Paciente e Exame.

**Requisitos**: 
O banco de dados deverá armazenar informações sobre os vários exames de um
determinado paciente, com o resultado e o valor pago.
Um paciente pode ter varios médicos e um médico pode ter varios pacientes.
Um paciente poderá fazer quantos exames quiser, mas cada exame é unico.

**Atributos:**
- 1. Médico: Nome e Especialidade;
- 2. Paciente: Nome, Endereço;
- 3. Exame: Tipo Exame, Aceita Convênio, Requisitos, Valor exame. 

**Obs: É necessario alterar ou incluir atributos de acordo com a necessidade**


**2) Projetar um Banco de Dados satisfazendo as seguintes restrições e requisitos:**
- 1. Para um Vendedor, armazenar seu código, nome, endereço e comissão;
- 2. Para um cliente, armazenar o seu código, nome, endereço. Além disso, armazenar o código e o nome do vendedor que o atende. Um
vendedor pode atender muitos clientes, porém um cliente deve ter apennas um vendedor;
- 3. Para uma peça, armazenar seu código, descrição, preço quantidade em estoque e o
número do armazém onde a peça está estocada. Uma peça somente pode estar estocada num
único armazém mas um armazem possui várias peças. 
- 4. Para um armazém, armazenar seu código e endereço.
- 5. Para um pedido, armazenar seu número, data, código, nome e endereço do cliente, que
fez o pedido e o código do vendedor para cálculo da comissão. **Há somente um cliente por
pedido e um vendedor;**
- 6. Um pedido pode ter varias peças e uma peça pode estar em varios pedidos


**Entrega: 26/02/21, sexta-feira**
