## Apresentação do Curso Preparatório para o Estágio

### Módulo Metodologia Ágil


Objetivo: Treinar o aluno nas melhores práticas e ferramentas mais usadas no mercado de TI;
   * Introdução ao Agile Scrum
   * Papo Ágil - Scrum

### Material de Estudo:

* [Introducao agile scrum](https://www.voitto.com.br/digital/introducao-agile-scrum)
* [Papo Ágil - Scrum](https://www.youtube.com/playlist?list=PL6011C890366AF5BC)
* [Agile Methodology](https://www.freecodecamp.org/news/complete-guide-to-agile-methodology/)
* [What is Kanban?](https://www.freecodecamp.org/news/what-is-kanban-the-agile-methodology-defined-and-how-to-use-it-for-your-software-development-team-2/)
* [Scrum Master](https://drive.google.com/file/d/12K5yem4NXGSBpuf3bUc2qqJxhZho-OUH/view)
* [E-Book Kanban](https://drive.google.com/drive/folders/1PZPhmmyQDQT8iiMfeEN3TfvK0bB6LWyd)
* [Guia Scrum](https://drive.google.com/drive/folders/1PZPhmmyQDQT8iiMfeEN3TfvK0bB6LWyd)


Tempo do Módulo: 10 dias corridos do início do curso
Duração do Curso: 3h