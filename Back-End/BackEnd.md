## Apresentação do Curso Preparatório para o Estágio


## Módulo Backend

Tempo de Execução do Curso: 15 dias corridos do início do curso
Objetivo: Treinar o aluno nas melhores práticas e ferramentas mais usadas no mercado de TI.

* Algoritmo;
* Javascript;
* Node.JS; 
* Express.
* Sequelize

### Material de Apoio: 

- [Algoritmo] (https://www.cursoemvideo.com/course/curso-de-algoritmo/)
- [JavaScript] (https://www.cursoemvideo.com/course/javascript/)
- [Node.js](https://www.youtube.com/watch?v=LLqq6FemMNQ&list=PLJ_KhUnlXUPtbtLwaxxUxHqvcNQndmI4B
- [Sequelize] (https://www.sequelize.org/master)


Tempo do Módulo: 15 dias corridos do início do curso

